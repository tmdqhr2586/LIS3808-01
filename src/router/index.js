import Vue from 'vue';
import Router from 'vue-router';
import Home from '../components/pages/Home';
import KoreanMusical from '../components/pages/KoreanMusical';
import MusicalDetail from '../components/pages/MusicalDetail';
import Theater from '../components/pages/Theater';
import NotFound from '../components/pages/NotFound';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/koreanMusical',
      name: 'KoreanMusical',
      component: KoreanMusical,
    },
    {
      path: '/theater',
      name: 'Theater',
      component: Theater,
    },
    {
      path: '/musicalDetail/:DBPage/:musicalID',
      name: 'MusicalDetail',
      component: MusicalDetail,
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound,
    },
  ],
});
