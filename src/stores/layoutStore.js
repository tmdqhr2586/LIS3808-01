const savedIsNavCollapsed = JSON.parse(window.localStorage.getItem('isNavCollapsed')) || false;

export default {
  namespaced: true,
  state: {
    isNavCollapsed: savedIsNavCollapsed,
    toolbarActive: '1',
    isHome: false,
  },
  mutations: {
    updateCollapse(state, { isNavCollapsed }) {
      state.isNavCollapsed = isNavCollapsed;
      window.localStorage.setItem('isNavCollapsed', isNavCollapsed.toString());
    },
    updateToolbarActive(state, { key }) {
      state.toolbarActive = key;
    },
    updateIsHome(state, { isHome }) {
      state.isHome = isHome;
    },
  },
};
