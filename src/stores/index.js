import Vue from 'vue';
import Vuex from 'vuex';

import layout from './layoutStore';
import auth from './authStore';
import search from './searchStore';
import theater from './theaterStore';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    layout,
    auth,
    search,
    theater,
  },
});
